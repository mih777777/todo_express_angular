const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')
const mongoose = require('mongoose')
const TodoSchema = require('./model.js')

let app = express()
mongoose.connect("mongodb://mih777:mih777@ds237337.mlab.com:37337/todo", { useNewUrlParser: true })
const Todo = mongoose.model("Todo", TodoSchema)

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/api/todo', (req,res) => {
    Todo.find({}, (err, todo) => {   
        if(err){
            res.send(err)
        }
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
        res.json(todo)
    }).sort({ created_date: -1 })
})

app.post('/api/todo', (req,res) => {
    let newTodo = new Todo(req.body)

        newTodo.save((err, todo) => {
            if(err) {
                res.send(err)
            }
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
            res.json(todo)
        })
})

app.delete('/api/todo/:id', (req,res) => {
    Todo.deleteOne({ _id: req.params.id }, (err, todo) => {
        if(err){
            res.send(err);
        }
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept')
        res.json({ message: 'Successfully deleted todo!'})
    })
})


if(process.env.NODE_ENV === 'production') {
    app.use(express.static('client/dist/client'))

    app.get('*', (req, res) => {
        res.sendFile(
            path.resolve(
                __dirname, 'client', 'dist', 'client', 'index.html'
            )
        )
    })
}



let PORT = process.env.PORT || 3000

app.listen(PORT, () => {
    console.log('Server listening on port' + PORT)
})