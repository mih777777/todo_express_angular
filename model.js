const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TodoSchema = new Schema({
    todoName: {
        type: String,
        required: 'Enter a name of todo',
    },
    categoryName: {
        type: String,
        required: 'CategoryName',
    },
    desc: {
        type: String,
        required: 'CategoryName',
    },

    created_date: {
        type: Date,
        default: Date.now
    }
})

module.exports = TodoSchema