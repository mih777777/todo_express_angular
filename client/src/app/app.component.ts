import { Component, OnInit } from '@angular/core'
import { HttpClient } from '@angular/common/http'

export interface Todo {
  categoryName: string
  todoName: string
  desc:string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{
  togle = false
  togle2 = false
  todos: Todo[] = []

  todoName = ''
  categoryName = ''
  desc = ''

  constructor(private http: HttpClient){

  }

  ngOnInit(){
    this.http.get<Todo[]>('/api/todo')
      .subscribe(todos => {
        console.log(todos)
        this.todos = todos
        
      })
  }

  addTodo(){
    if(!this.todoName.trim()) {
      return
    }

    const newTodo: Todo = {
      todoName: this.todoName,
      categoryName: this.categoryName,
      desc: this.desc
    }
    this.http.post<Todo>('/api/todo', newTodo)
      .subscribe(todo => {
        console.log('todo', todo)
        this.todos.push(todo)
        this.togle = false
         //alert('Successfuly edded !')
         this.todoName = ''
         this.categoryName = ''
         this.desc = ''
      })

  }

  updateTodo(){
    

  }

  removeTodo(id: number){
    this.http.delete<void>(`/api/todo/${id}`)
      .subscribe(resp => {
        console.log(resp)
      })
  }

}
